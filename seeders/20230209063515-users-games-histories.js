'use strict';

const { v4: uuidv4 } = require('uuid');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('UserGameHistories', [
      {
        user_id: 1,
        uuid: uuidv4(),
        score: '10 - 1',
        result: 'WIN',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: 1,
        uuid: uuidv4(),
        score: '12 - 1',
        result: 'WIN',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: 1,
        uuid: uuidv4(),
        score: '1 - 10',
        result: 'LOSE',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
    */

    await queryInterface.bulkDelete('UserGameHistories', null, {});
  }
};
