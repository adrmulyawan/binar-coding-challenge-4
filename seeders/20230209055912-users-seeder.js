'use strict';

const { v4: uuidv4 } = require('uuid');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const myPlaintextPassword = '12345678';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('Users', [
      {
        uuid: uuidv4(),
        full_name: 'Adrian Mulyawan',
        email: 'adrianmulyawan666@gmail.com',
        username: 'adrianmulyawan',
        password: await bcrypt.hash(myPlaintextPassword, saltRounds),
        role: 'USER',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: uuidv4(),
        full_name: 'Hakaman Sujatmoko',
        email: 'hakamansujatmoko@gmail.com',
        username: 'hakaman99',
        password: await bcrypt.hash(myPlaintextPassword, saltRounds),
        role: 'USER',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: uuidv4(),
        full_name: 'Rifqi Afif',
        email: 'rifqiafif@gmail.com',
        username: 'rifqiafif',
        password: await bcrypt.hash(myPlaintextPassword, saltRounds),
        role: 'USER',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        uuid: uuidv4(),
        full_name: 'Muhammad Sumbul',
        email: 'muhsumbul@gmail.com',
        username: 'admin1',
        password: await bcrypt.hash(myPlaintextPassword, saltRounds),
        role: 'ADMIN',
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Users', null, {});
  }
};
