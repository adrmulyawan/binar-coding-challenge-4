'use strict';

const { v4: uuidv4 } = require('uuid');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('UserBiodata', [
      {
        user_id: 1,
        uuid: uuidv4(),
        full_name: 'Adrian Mulyawan',
        username: 'adrianmulyawan',
        email: 'adrianmulyawan666@gmail.com',
        phone_number: '082154590559',
        address: 'Jl Kaliurang Sleman',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: 2,
        uuid: uuidv4(),
        full_name: 'Hakaman Sujatmoko',
        username: 'hakaman99',
        email: 'hakamansujatmoko@gmail.com',
        phone_number: '081258161143',
        address: 'Jl Kaliurang Sleman',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: 3,
        uuid: uuidv4(),
        full_name: 'Rifqi Afif',
        username: 'rifqiafif',
        email: 'rifqiafif@gmail.com',
        phone_number: '081258161143',
        address: 'Jl Kaliurang Sleman',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: 4,
        uuid: uuidv4(),
        full_name: 'Muhammad Sumbul',
        username: 'admin1',
        email: 'muhsumbul@gmail.com',
        phone_number: '081258161143',
        address: 'Jl Kaliurang Sleman',
        createdAt: new Date(),
        updatedAt: new Date()
      }
     ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
    */

    await queryInterface.bulkDelete('UserBiodata', null, {});
  }
};
