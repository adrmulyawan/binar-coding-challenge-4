'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasOne(models.UserBiodata, {
        foreignKey: 'user_id',
        as: 'biodata'
      });
      User.hasMany(models.UserGameHistory, {
        foreignKey: 'user_id',
        as: 'histories'
      });
    }
  }
  User.init({
    uuid: DataTypes.UUID,
    full_name: DataTypes.STRING,
    email: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.ENUM('USER', 'ADMIN')
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};