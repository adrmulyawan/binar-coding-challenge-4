const express = require('express');
const router = express.Router();
const models = require('../models');
const User = models.User;
const UserBiodata = models.UserBiodata;

router.get('/', (req, res) => {
  // console.log(req.session);
  res.render('pages/index', {
    layout: 'layouts/main-layouts',
    title: 'Temple Run',
    username: req.session.username,
    checkRole: req.session.role,
    userUUID: req.session.uuid,
  });
});

router.get('/game', (req, res) => {
  res.render('pages/game', {
    layout: 'layouts/game-layouts',
    title: 'ROCK, PAPER, SCISSORS GAME',
    checkRole: req.session.role,
    userUUID: req.session.uuid,
  });
});

router.get('/user/settings/:uuid', async (req, res) => {
  const data = await User.findOne({
    where: {
      uuid: req.params.uuid
    },
    include: ['biodata']
  });

  console.info(`Data User Login: ${data.username}`);

  res.render('pages/user-settings', {
    layout: 'layouts/auth-layouts',
    title: 'User Settings',
    username: req.session.username,
    checkRole: req.session.role,
    userUUID: req.session.uuid,
    data: data
  });
});


router.put('/user/settings', async (req, res) => {
  try {
    const {username, email, full_name, phone_number, address} = req.body;

    await User.update({
      full_name: full_name,
      email: email,
      username: username,
    }, { where: { id: req.body.id } });

    await UserBiodata.update({
      full_name: full_name,
      email: email,
      username: username,
      phone_number: phone_number,
      address: address
    }, { where: { user_id: req.body.id } });

    req.flash('successUpdate', 'Berhasil Update Data User!');
    res.redirect('/');
  } catch (error) {
    req.flash('failedUpdate', 'Gagal Melakukan Update Data User');
    res.redirect('/')
  }
});

module.exports = router;